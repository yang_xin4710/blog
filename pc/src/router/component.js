const Container = () => import ('@/components/Container')
const Main = () => import ('@/components/Main')
const hello00 = () => import ('@/components/HelloWorld00')
const CommonContainer = () => import ('@/views/view_Login')
const v_login = () => import ('@/components/login/Login')
const v_sign = () => import ('@/components/login/Sign')
const v_infos = () => import ('@/components/home/homeNews')
const v_downloadApp = () => import ('@/components/home/downloadApp')
const v_downloadClient = () => import ('@/views/view_DownLoadClient')

const v_MemberPwd = () => import ('@/components/account/pwd/pwd')
const v_userInfo= () => import ('@/components/account/info/userInfo')
const v_information= () => import ('@/components/information/information')
const  v_imPanel = () => import ('@/components/im/imPanel')
//
const v_author = () => import ('@/views/view_author')




/**
 * URI资源对应的路由组件,
 * @type {{path: string, name: string, component: (function(): (Promise<*>|*)), children: *[]}}
 */
const  URI = {
  path: '/home',
  name: '首页大厅',
  component: Container,
  children: [
  ]
}


const MemberPwd = {
  name: "登陆密码管理",
  path: "/home/account/pwd",
  component: v_MemberPwd,
  children: []
}

const UserInfo = {
  name: "个人资料",
  path: "/home/info/userInfo",
  component: v_userInfo,
  children: []
}

const IM_SERVICE = {
  name: "在线客服",
  path: "/home/im/imPanel",
  component: v_imPanel,
  children: []
}
const  AuthorizationURI = {
  path: '/home',
  name: '首页大厅',
  component: Container,
  beforeEnter: (to, from, next) => {
    //此处可以添加一些在进入需要登录后才能访问的判断
    console.log(666);
    next();
  },
  children: [
    MemberPwd,
    UserInfo,
    IM_SERVICE,
  ]
}

export default [
  {
    path: '/',
    redirect: '/home',
    // redirect: '/author'
  },
  AuthorizationURI,
  {
    path: '/login',component:CommonContainer,
    children:[{path:"",name:"登陆",component: v_login}]
  },
  {
    path: '/sign',component: CommonContainer,
    children: [{path: '/sign',component: v_sign,name: "会员注册"}]
  },
  {
    path: '/downloadApp',component: CommonContainer,
    children: [{path: '',component: v_downloadApp,name: "app下载"}]
  },
  {
    path: '/downloadClient',component: v_downloadClient,name: "app下载"
  },
  {
    path: '/info',component: CommonContainer,
    children: [{path: '/info',component: v_infos,name: "公告列表"}]
  },
  {
    path: '/info/information',component: CommonContainer,
    children: [{path: '/info/information',component: v_information,name: "详情"}]
  },
  {
    path: '/author',component: v_author,name:"作者主页",
    children: []
  },
  {
    path: '/404',component: hello00,name: "互联网的荒原",
    children: []
  },
  {
    path: '*',redirect: "/404", children: []
  }
]
