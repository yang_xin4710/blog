/**
 * 需要录入系统的菜单URI
 * 2、默认的PC端用户中心菜单栏
 */
export default [
  {
    "id": "1",
    "name": "账户管理",
    "path": "",
    "type": 1,
    "children": [
      {
        "id": "1-1",
        "name": "登陆密码",
        "path": "/home/account/pwd",
        "type": 2,
        children: []
      },
    ]
  },
  {
    "id": "2",
    "name": "团队管理",
    "path": "",
    "type": 1,
    "children": [
      {
        "id": "2-1",
        "name": "我的邀请码",
        "path": "/home/account/inviteCode",
        "type": 2,
        children: []
      },
      {
        "id": "2-2",
        "name": "推荐注册",
        "path": "/home/account/recommend",
        "type": 2,
        children: []
      },
    ]
  },
]
