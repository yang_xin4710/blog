// import axios from 'axios' //idnex.html首页引入之后。这里不需要了
// import Vue from 'vue' //idnex.html首页引入之后。这里不需要了
// import Qs from Qs'
import router from '../router/routerscfg'
import store from '@/vuex/store'
import {Message} from "element-ui"
import URLS from "./interface"
import tableConfig from "./tableConfig"

//系统提示常量
var Message_STATUS={
  "serviceClose":"服务器已关闭",
  "serviceUnKnowError":"请求未知错误",
  "serviceNotFound":"找不到该服务",
}

var msgError = function (msg){
  Message.error({
    message:msg,
    duration: 2500
  })
  throw new Error(msg);
}
var msgWaring = function (msg){
  Message.warning({
    message:msg,
    duration: 2500
  })
}

var instance = axios.create({
  baseURL: process.env.BASE_API, // api的base_url
  timeout: 60000,
  withCredentials: true
})

instance.interceptors.request.use(config => {
  // console.log("请求开始前"+Qs.stringify(config));
  // // console.log("请求前的laoding0000000",store.state.loadingMap)
  return config
}, error => {
  // console.log("请求前错误"+error);
  return Promise.reject(error);
})

instance.interceptors.response.use(res => {
  // console.log(res);
  store.commit("clearLoadingMap",res.config.url);
  // console.log(res.config.url);
  // console.log(store.state.loadingMap);
  // console.log("返回结果前"+res.data);
  return checkStatus(res);
}, error => {
  // console.log("返回报错");
  console.log(error);
  console.log("返回报错");
  if(error && error.response){
    store.commit("clearLoadingMap",error.response.request.responseURL);
    if(error.response.status===404){
      msgError(Message_STATUS.serviceNotFound);
    }else{
      msgError(error.response.data.message);
    }
  }else{
    store.commit("clearLoadingMap");
    // msgError(Message_STATUS.serviceClose);
  }
})

//检查返回状态
function checkStatus(res) {
  //每交互一次就更新一下当前本地的过期token时间；不需要了
  // if(res.config.url.indexOf(URLS.login)==-1){
  //   store.commit("updateToken",res.data);
  // }
  if ((res.status == 200 || res.status == 304)) {
    if(res.data.code==0){
      return res.data;
    }else if(res.data.code>=1000&&res.data.code<=1099){
      //需要需要清除登陆
      store.commit("clearToken");
      router.replace("/login");
      if(res.data.serviceTime-store.exptime>5000){
        store.exptime=res.data.serviceTime;
        msgWaring(res.data.message);
      }
    }else{
      if(res.data.code==10001){
        if(router.length>1){
          router.back(-1);
        }else{
          router.replace("/home");
        }
      }
      if(res.data.data!=null){
        msgWaring(res.data.data);
      }else{
        msgWaring(res.data.message);
      }

    }
  }else{
    msgWaring(Message_STATUS.serviceUnKnowError);
  }
}

/**
 * IE 浏览器的get请求会主动根据url缓存结果。导致相同的url不更新请求数据。在末尾加上一个时间戳
 */

export default {
  RESOURCE_URL:process.env.RESOURCE_DOMAIN,
  BASE_URL:process.env.BASE_API,
  WS_DOMAIN:process.env.WS_DOMAIN,
  defaultimgbase64:`data:image/gif;base64,R0lGODlhPAA8APU4AP7+/v39/fz8/Pv7+/r6+vf39/n5+fj4+MjIyPb29vX19cnJyfT09PHx8fLy8u3t7crKyubm5u/v7+zs7PPz8+vr6+np6efn5+jo6O7u7svLy/Dw8M3Nzc/Pz+rq6tDQ0MzMzNfX19HR0ePj487OztjY2OTk5OHh4dTU1N3d3eLi4uDg4NnZ2dbW1uXl5dvb29PT09/f39ra2tLS0tXV1d7e3tzc3P///9vtsgAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh+QQFAAA4ACwAAAAAPAA8AAAG/8CbcEgsGo/IpHLJbDqf0Kh0Sq1ar9isdstELD6h06TAzXq/kHQJcyhXz2gNaG5KuKVw9ZxDQrHvAgZkbYIDAYdGDyYyfXyNKRSAdoSITBcljyItD24Kk4FRFixgmjAVW5EMngBvpTB+WQ6yq1cVYSi4ErUNs1uMNJu0obobvb4tt5VQGczEbrcsnMPSkpijlE5j1IBijKdP2tqSEi/lMQTgHurKd4spkOkWwtU19dmX8pJEKzH96JYRLjDTN0QUP1UAAxLc1y1hnYUFVYzw4LAQxIkPl2BUAeqii48VIQoJqFAjvxWsROITaNKfyABs/rSsgY2gwTEzU3lcl/NlOHuHGRcqEtdzZ0clEzHhFDrQiTWUTJ01+RXMJi92SJEBGzTOmFNcpqxibfnqm8+pM9JW7cQVj6ttvo66JZXvJdoOeDlwtHs30z++SSa8ciSnJGAk5fbowfj38EhSeuIgcFxERSPJXignCoF5suYjtjh/+Uy6tOnTqFPbDQIAIfkEBQAAOAAsBgAGADAAMAAABv/Am3BILA4nJ9aMA2KibKbMwUitWoWR0GIL0TSdnY9oWTFcz1WEmuv9hsXkEqaArgOE624bDIc9WzITAXZoa1EMBHeJEhc1NH6PWoeElFUejoByNQ2VAwKfg50PMZqBEqKKnVhKL1AbqKpHpCmODrCxq7V0t7FJK7+huL2+nJaLwpYqyhHHRwpTyFcWI5POiJ7R0i7boGYU36nZVBVZF9fPtt7iyY1Sg5zp6+zT3a/y2pd07rv387b7/fyNeoAtoJGBjJAIMiiwDMNxCemFe4jlFb05FK3Ba9cs4z+ECTxq3MZN5DkFUaqZ9ESNmrqV05bBzAVspSxS9mwO1KXTlytHmzRPAS1nCmgCTJqMspKkVBINZj0j+XEI80+ckIXE/Voy5k3JVAq9rJvW9Q0fPWzUyNtm9mzaNf3Iun0LqNu9gVzRJuFnJwgAIfkEBQAAOAAsBgAGADAAMAAABv/Am3BILA4fI1tLxETJVpeNwUitWoUYFmfb+XhnMOeypMoQrmjjqQtqk9hNcYj8rEzTVxfc/d7L6S9KEgF4VCULiBAai4FmAwCQCQ0eKk+WKTUYBYVEfYlzCnhIgjFQFJxDCKpOZ6gSJplQZa43CDGhtFiySae5vrqVsLi/vlEuERZ3xLlZyI7LzMYM0MXJE4/UtJR22dHPAgfhrd1V1w+b5Jzn35Lo6eWvhMPv8FLj8/Rqk+3T+fW4vfzpC+WgYCSBRabta4Dwn72GCeMNghjRDDuK4tZhzMjAHL6Ndh6C3OYB20Zg/U4qaHZNJThnAUceS+bSWJKavE4wxGmKZylMgzJjTQzaSBwzBEcDsejpKoyqpIC4pdGzahXUMUs04RvF5VOIndqU0vjzpSsjrybAEWtG1s9ZRDq7VajhtKxZRWSMpnuFye5Sj5yCAAAh+QQFAAA4ACwGAAYAMAAwAAAG/8CbcEgsDjORGovGLKVGHgfBSK1ahZbXR8SFoZpOmTY5uJqNoxmps+1+W6HwWJkxnM1Jjp7d9oKXczErJg0Bd2hLIIprahcSCWUAkRMugpYqUAeHR32LGJBnj4OYhAqbEQsQqhpwU5sCUlCVEQx3qKmqg6+IZKZmCMCpn7tUWZ8WBWfBFcRWFcYPrlckzM3OUZSS1tuQ2aDc1tF1sODhouXmG4Xozerr7MSx8O0Uhnb383i1k+T51/ia/Nn6JlAfwYL/fCFMuG9hsXoQHTKU+PAdRX7ukl3sd+/RRo5Ixn0E6W1kt2omYT2LkpICNnEtj8V0RGtmJYUfj2WCt2WgrFJS5RghOPiQlCCAu2LgAlZT36gnHl9h+LK0qS06NsZoxANnKbV4TwKROohEDKtVwswF6uqn06KzX8F5yCrnjRuhcHEGZduWz54lAQU6StRn56sgACH5BAUAADgALAYABgAwADAAAAb/wJtwSCwSMaraa6lEMgbGqHQqrMRorVCJJWM2T0kKlEouRrIorJbrvYJHpktjXJ4iRTNYWr1tr94ugRZPdVE2HR94eVoec3QHT41xZ04EhUNWJIiJNo2FVnKDD5CfHKabFY+fohOtdRcgsaZwl4agGQVkFRq8sYO1UrjCBnaJvRPAuhIbDqpDXBC8yMnKjkYuC9kQGNTVzcRFmtkr3asKAUcI6ijn5d7gROpy7ub09AnO9vr7/P3+/wICAhxIsKDBgwhJJUTHMJfDhQ3xSYQIoOJEihbbPcQYKSJHeB8JhWRWcJ6tZQIB6kFwctTAK+s0Yho1zR+sdSaCtaq5L1NMV52SLrrzmc1aS0/3jBX1RslStxW9okVYJegXMAtoom2r5QTOG5DFTh1LthPQFZ4Zb24S+41s1S9d1uxRtFbE1KRw2WRdpJRWT0px5a5UZFWl16xnFRYKAgAh+QQFAAA4ACwGAAYAMAAwAAAG/8CbcEgsEh8Y12gZsSALAqN0Sh0qa7HVScVsJj2bRKBKNnpSL5s6u+1evpVJRlwmy0ssWRrNdsOfDQcAdWZteHpYcwZRgwRQYXKKgYyEQnktIZlbgnWTkBSOhGcopJiSlYKeDKF2MzCvNE2ohasKA60iriVys1S2dFQ0H7m7vXaPU2odw4rGzhck0R1gztW6JCPV1iAc3cDas8PcTuC90BrcrOWVeOgR670Q8hy/8KIL+Cz2qCf52fuEMuHjBRAXAgS1CipcyLChw4cQI5o7+KLerYgjDiJY0egixHYIO6priO4gskWUGLrI10IiEWEDXVpiycklzAUnkqEsuGLeNGidI9dhKAnBBVBQ+yYQQ5fyiIOnUIWWSnfsE7hR3UBUqCrh1LNl0jCIktR1TLil2MJFIheUFixxBPmBcfImrsg4IK8hNbbWixs+lzBNZatt7hVDgAMLk2WvrxYsawLbTdrVxGM+1GYFAQAh+QQFAAA4ACwGAAYAMAAwAAAG/8CbcEgsEh2SR2WibFAKhIBxSq0OmReMxbNsbpzIqHVsTI5MrkiWizWHGQkxeXxW1dVadsb9hMcHc1RrJ3ZoXlJlfn9ygUKENTErhG+NUAaMcy82KZBqApUAgAejmHSam0ygqkcsrZpJq7EKp61hsqqPJTZOt7g0LcB7vb6/F8PEKCmWx43AMDCpzIEeIjPWotKZ1SIW2c0f4IveYx3lyuNkIyTrEehVLwiSHPPC7kcI+CHP9PZG+eYgQJSy90+DQQ2h+hEpeDChQiELIkqEAGGgu4nbKj4cglFitIcUKZ7wuPGGiZCP8pWU0XBZyVktXzoKGFAmx3kCbZpcx8+mK1USJnSqMyduY4WMGIRaq/ZJJsulfV7+nPHAqb5fyXApuIQsmNaoTU3R0gfr65ey2BCZ4TR26y0we9oMKhSJ7bljfPTgodtpiTcvW+bm+tLv7Fy0jYIAACH5BAUAADgALAYABgAwADAAAAb/wJtwSCwaKQ6konAgCADHqHSakWyuScYywTQMAtCp+Dh5mK2N7Lbr/YbHVIunUq5ildxm2/2GF89ydHZOfU+Eh4Z+RBgRF4yBfIpgk4omlo1nkpp/KiOXWJuhQieknniim56la6iajjErnq2iKTW2abOpNrVluVMIHKxzL7t7vn/ACCuLMs1Wx2TJwaMs1SqJ0MzJEdQlJWbZ0cndLGzh2gsLTC3s1+fi6hc08xbv8KUoKFf2yBD+1jDy8CPyTwSMGQgjDSSgoSEIgwYLDXRI8YNEfg8zftiIaCGHjyTmdejAyiNIYiGrLCSH8qOLlSwfnIR5oxbHAhpL2st301tDaG4rEUasSbHjOZszKgwJ+c/kwT0n0qXTCQ3pBXTK7L2i1w8BxhBglXKaCBAbzFjNbFAli3IfzTK2ir1NgpbXXFVos8wyJ9HOp1h8aQlDU+eRi0+U9g72OwzTs3Ba1DCGpPDonUFmxwQBACH5BAUAADgALAYABgAwADAAAAb/wJtwSCwaC8iDgTAIAI7QqFRBqVKTS2bTOe0eG2BrYqzUbrler2QdZlyxZvQz/Z08MmyxfL7n04d2gXhlf35/Fh6JgX2FhxiIFW6NURFdEReYkZNRCAgWUy6XiZt1nSRgdSahZKRFFSSmIQKpqq2csUaqKiNrtqULwG9CKyfFcb5FKMALJ0S7xA7IlMunQzXXldK3EBAXwyngvdpf3NzfNjGE464cGu5IL/HZ6+TviTL49L8gzyws0fpygRiIr4RBOAGHtFsYouGZhAphdWhBsYUhiBMz0th4MaHGjcoeQhzwoaQIfzBgsBqZ4KTLGTDFsYQZ8yXLcyoz2LxJLOQBapMzVnoEuSReRp4VcZY4Rq+Yw4jtMIxEKSMOQYtD/X0i8nJFwHv52Lnr1hQcujsCy2nS1m+e2GVrfenCNm0ZW6fpQHW6+wzVTSqiePlFGljSzEiQRDUVtiGPoq2LGTteFFnyIKGV9WT5EwQAIfkEBQAAOAAsBgAGADAAMAAABv/Am3BILBoHBKRgCTg6n1DlYZqUBq7NqHZrqDKxWa6YoUgUzl0vODyGOihkc5oNRYUa7TQeLs8j/hZ5G3tqY3+HEVwPGYyDa36HC4lRE5WNeUYXJJEelJZzmEQVIgulHYSZHhWLoU8dpixfoha0fa2ZELkQk7MYvrd1uh9lvZXAwRoaI8Vxx04YySDDQybVq87IHBhCtCPL2G4c4jLcKuaF4LObJFMrJ+/p2Ysx7hLxTxGvw5o1Mc33Rj4ITPGOIBWAAUmFIGjDhiyE5WZIfEHxBR2IAGBo3CiDHMYidkKyGGnlI42TLRiWQPNRSMqXC++0jLgQ2suZN0aVWLkB5Ql7nNVIGhBpC6LKLvQ0BmrZkCJNhyYZ2nOpsBPGfgapVS2aLh89Vk9VGK0HKqc+dgC9wTvScNyEeBeCWmU7bsZbbKqsscQlLdm2Y3ldCKYULZe/W4F5ERYG40KoVd3+LTYlqBFkrpMrW0blZwwfR1OZEnsDGujo06bLcgkCACH5BAUAADgALAYABgAwADAAAAb/wJtwSCwaAcjkcclsOpU3D6IVKTyvWOJ0e1Jkv4EwVLgtuwTgtFZTXrgbauvBQBc/XSi3/lOJz9FgF3kQhBoXgQkEY18rhWwYdxQMXgN+QxgibCAgfU0OkomWRBMwmxwzn0twqaJFpKckNoCuG6utRhawHZC0ErW3RyodwyihlxnIs8BrH3y9csuuzZl1iRPXdtHMqMed2rgzpTVCFuXe30VUeXQY7b7o4DQ0tRf1oPCj6u5V0PjkIQBV7FPkj1kKPCZMKCvIoqGMERBHLML3sKJAFRPhvdjI8USXgvlsHFxBshrIkR5jqJwE8l8Ney8ftHQpE+VMcysmidz4h+HFc0oJK7aMaMFlw0r+KiBktTOEzKRVDl0KOI4izJDqrLqTJq8YOg9giwYb5FRbMrAZS4gru+wBNoJjwzXzAMyX256qMk3DKIpesjvTYrH18yvLK12DsbC0FQhxIYiRFvvR6whBZEp9iemZyfVNRpNfc34GEwQAOw==`,
  getAllJson(reqArr) {
    return axios.all(reqArr);
  },
  getJson(url,params) {
    // console.log("get带参",params==undefined,params==null);
    if(params==undefined||params==null){
      return instance({
        method: 'get',
        url:url+"?t="+new Date().getTime(),
        headers: {
          //'X-Requested-With': 'XMLHttpRequest'
          'CSRFToken':store.state.CSRFToken
        }
      })
    }else{
      return instance({
        method: 'get',
        url:url+"?"+Qs.stringify(params,{allowDots:true})+"&t="+new Date().getTime(),
        // params:params,
        headers: {
          //'X-Requested-With': 'XMLHttpRequest'
          'CSRFToken':store.state.CSRFToken
        }
      })
    }
  },
  postJson(url, data) {
    return instance({
      method: 'post',
      url:url,
      data: Qs.stringify(data),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        'CSRFToken':store.state.CSRFToken
      }
    })
  },
  postJsonBets(url, data) {
    //投注专用提交接口,规则下的所有投注项全部提交上去。若为空，代表此项没选择。所以不处理。
    var t = Qs.stringify(data)+"&";
    var flag=false;
    for(var i = 0;i<data.bets.length;i++){
      if(data.bets[i].length==0){
        flag=true;
        t+="bets["+i+"]=&";
      }
    }
    if(flag){
      t = t.substring(0,t.length-1);
    }
    console.log(t);
    return instance({
      method: 'post',
      url:url,
      data: t,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        'CSRFToken':store.state.CSRFToken
      }
    })
  },
  postJsonObject(url, data) {
    return instance({
      method: 'post',
      url:url,
      data: Qs.stringify(data,{allowDots:true}),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        'CSRFToken':store.state.CSRFToken
      }
    })
  },
  postFile(url, data) {
    return instance({
      method: 'post',
      url:url,
      data:data,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'CSRFToken':store.state.CSRFToken
      }
    })
  },
  postJsonAndFile(url, data) {
    return instance({
      method: 'post',
      url:url,
      data:data,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        'CSRFToken':store.state.CSRFToken
      }
    })
  },
  URLS:URLS,
  tableConfig:tableConfig
}
