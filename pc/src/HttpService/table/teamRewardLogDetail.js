export default {
  userName: {
    params:{
      value:"",
      column:"userName",
      condition:"LIKE",
      prefix:"`t_sys_user`."
    },
    orderBy:{
      state:0,
      index:0,
      value:'',
      options:[]
    },
    alias:'包含',
    state:0,
    label:"用户名",
    type:1,
    conditions:{
      options:[
        {opt:"LIKE",alias:"包含"},
        {opt:"=",alias:"等于"}
      ]
    }
  },
  expect: {
    params:{
      value:"",
      column:"expect",
      condition:"LIKE",
      prefix:"`t_sys_member_bet`."
    },
    orderBy:{
      state:0,
      index:0,
      value:'',
      options:[]
    },
    alias:'包含',
    state:0,
    label:"游戏期号",
    type:1,
    conditions:{
      options:[
        {opt:"=",alias:"等于"},
        {opt:"LIKE",alias:"包含"}
      ]
    }
  },
  gameId: {
    params:{
      value:"",
      column:"gameId",
      condition:"=",
      prefix:"`t_sys_member_bet`.",
      isString:false
    },
    orderBy:{
      state:0,
      index:0,
      value:'',
      options:[]
    },
    alias:'等于',
    state:0,
    label:"游戏名称",
    type:3,
    conditions:{
      options:[
      ]
    }
  },
  serialNo: {
    params:{
      value:"",
      column:"serialNo",
      condition:"LIKE",
      prefix:"`t_sys_member_bet`."
    },
    orderBy:{
      state:0,
      index:0,
      value:'',
      options:[]
    },
    alias:'包含',
    state:0,
    label:"流水号",
    type:1,
    conditions:{
      options:[
        {opt:"=",alias:"等于"},
        {opt:"LIKE",alias:"包含"}
      ]
    }
  },
  betType: {
    params:{
      value:"",
      column:"betType",
      condition:"=",
      prefix:"`t_sys_member_bet`."
    },
    orderBy:{
      state:0,
      index:0,
      value:'',
      options:[]
    },
    alias:'',
    state:0,
    label:"投注类型",
    type:3,
    conditions:{
      options:[
        {opt:"1",alias:"余额"},
        {opt:"2",alias:"代币"}
      ]
    }
  },
  createTime: {
    params: {
      column: "createTime"
    },
    orderBy: {
      state: 1,
      value: 'desc',
      options: [
        {opt: 'desc', alias: '降序'},
        {opt: 'asc', alias: '升序'}
      ]
    },
    label: "投注时间",
    type: 2,//时间筛选类型
    filter: [
      {
        params: {
          value: "",
          column: "createTime",
          condition: ">=",
          prefix: "`t_sys_member_bet`.",
        },
        alias: '大于等于',
        state: 0,
        value: "00:00:00",
        label: "开始时间",
        conditions: {
          options: [
            {opt: ">=", alias: "大于等于"},
            {opt: "=", alias: "等于"},
            {opt: ">", alias: "大于"}
          ]
        }
      },
      {
        params: {
          value: "",
          column: "createTime",
          condition: "<=",
          prefix: "`t_sys_member_bet`.",
        },
        alias: '小于等于',
        state: 0,
        label: "结束时间",
        value: "23:59:59",
        conditions: {
          options: [
            {opt: "<=", alias: "小于等于"},
            {opt: "=", alias: "等于"},
            {opt: "<", alias: "小于"}
          ]
        }
      }
    ]
  },
  id:{
    params: {
      value: "",
      column: "id",
      condition: "=",
      prefix: "`t_sys_agent_reward_statistics_info`.",
      isString:false,
    },
    show:false,
    orderBy: {
      state: 0,
      index: 0,
      value: '',
      options: []
    },
    alias: '等于',
    state: 0,
    label: "记录ID",
    type: 1,
    conditions: {
      options: [
      ]
    }
  }
}
