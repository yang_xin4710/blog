export default {
  userName: {
    params:{
      value:"",
      column:"userName",
      condition:"LIKE",
      prefix:"`t_sys_user`."
    },
    orderBy:{
      state:0,
      index:0,
      value:'',
      options:[]
    },
    alias:'包含',
    state:0,
    label:"用户名",
    type:1,
    conditions:{
      options:[
        {opt:"=",alias:"等于"},
        {opt:"LIKE",alias:"包含"}
      ]
    }
  },
  serialNo: {
    params:{
      value:"",
      column:"serialNo",
      condition:"LIKE",
      prefix:""
    },
    orderBy:{
      state:0,
      index:0,
      value:'',
      options:[]
    },
    alias:'包含',
    state:0,
    label:"流水号",
    type:1,
    conditions:{
      options:[
        {opt:"=",alias:"等于"},
        {opt:"LIKE",alias:"包含"}
      ]
    }
  },
  type: {
    params:{
      value:"",
      column:"type",
      condition:"=",
      prefix:""
    },
    orderBy:{
      state:0,
      index:0,
      value:'',
      options:[]
    },
    alias:'',
    state:0,
    label:"账变类型",
    type:3,
    conditions:{
      options:[
        {opt:"0",alias:"新建记录"},
        {opt:"1",alias:"充值"},
        {opt:"2",alias:"提现"},
        {opt:"3",alias:"投注"},
        {opt:"4",alias:"投注返点"},
        {opt:"5",alias:"中奖"},
        {opt:"6",alias:"撤单"},
        {opt:"7",alias:"直推团队奖励"},
        {opt:"8",alias:"代币返现"},
        {opt:"9",alias:"冻结资金返还"},
        {opt:"10",alias:"资金调整"},
        {opt:"11",alias:"代币调整"},
        {opt:"12",alias:"冻结资金调整"},
        {opt:"13",alias:"冻结代币调整"},
        {opt:"14",alias:"注册奖励"},
        {opt:"15",alias:"下级团队奖励"},
        {opt:"16",alias:"快速折现"},
        {opt:"17",alias:"流水返点"}
      ]
    }
  },
  createTime: {
    params: {
      column: "createTime"
    },
    orderBy: {
      state: 1,
      value: 'desc',
      options: [
        {opt: 'desc', alias: '降序'},
        {opt: 'asc', alias: '升序'}
      ]
    },
    label: "账变时间",
    type: 2,//时间筛选类型
    filter: [
      {
        params: {
          value: "",
          column: "createTime",
          condition: ">=",
          prefix: "`t_sys_member_asset_log`.",
        },
        alias: '大于等于',
        state: 0,
        value: "00:00:00",
        label: "起始时间",
        conditions: {
          options: [
            {opt: ">=", alias: "大于等于"},
            {opt: "=", alias: "等于"},
            {opt: ">", alias: "大于"}
          ]
        }
      },
      {
        params: {
          value: "",
          column: "createTime",
          condition: "<=",
          prefix: "`t_sys_member_asset_log`.",
        },
        alias: '小于等于',
        state: 0,
        label: "截至时间",
        value: "23:59:59",
        conditions: {
          options: [
            {opt: "<=", alias: "小于等于"},
            {opt: "=", alias: "等于"},
            {opt: "<", alias: "小于"}
          ]
        }
      }
    ]
  }
}
