export default {
  createTime: {
    params: {
      column: "createTime"
    },
    orderBy: {
      state: 1,
      value: 'desc',
      options: [
        {opt: 'desc', alias: '降序'},
        {opt: 'asc', alias: '升序'}
      ]
    },
    label: "结算时间",
    type: 2,//时间筛选类型
    filter: [
      {
        params: {
          value: "",
          column: "createTime",
          condition: ">=",
          prefix: "`t_sys_agent_reward_statistics`.",
        },
        alias: '大于等于',
        state: 0,
        value: "00:00:00",
        label: "起始时间",
        conditions: {
          options: [
            {opt: ">=", alias: "大于等于"},
            {opt: "=", alias: "等于"},
            {opt: ">", alias: "大于"}
          ]
        }
      },
      {
        params: {
          value: "",
          column: "createTime",
          condition: "<=",
          prefix: "`t_sys_agent_reward_statistics`.",
        },
        alias: '小于等于',
        state: 0,
        label: "截至时间",
        value: "23:59:59",
        conditions: {
          options: [
            {opt: "<=", alias: "小于等于"},
            {opt: "=", alias: "等于"},
            {opt: "<", alias: "小于"}
          ]
        }
      }
    ]
  },
  level:{
    params: {
      value: "",
      column: "level",
      condition: "=",
      prefix: "`t_sys_agent_reward_statistics`.",
      isString:false,
    },
    orderBy: {
      state: 0,
      index: 0,
      value: '',
      options: []
    },
    alias: '等于',
    state: 0,
    label: "代理等级",
    type: 3,
    conditions: {
      options: [
        {opt:3,alias:'初级代理'},
        {opt:4,alias:'中级代理'},
        {opt:5,alias:'高级代理'},
      ]
    }
  }
}
