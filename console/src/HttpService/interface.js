var openUrl = {
  getLoginCaptcha:"sys/captcha/getLoginCaptcha",
  login:"sys/user/login",
  logout:"sys/user/logout",
  getAllSysConfig:"sys/config/getAllSysConfig"
}

var imUrl={
  addService:"sys/im/addService"
}

var userUrl = {
  lockSysUserAccount:"sys/user/lockSysUserAccount",
  delSysUserAccount:"sys/user/delSysUserAccount",
  lockSysUserAdminLoginPerm:"sys/user/lockSysUserAdminLoginPerm",
  unlockSysUserAccount:"sys/user/unlockSysUserAccount",
  addSysUserAccount:"sys/user/addSysUserAccount",
  findAllAdminUserInfoBaseVo:"sys/user/findAllAdminUserInfoBaseVo",
  findAllUserInfoBaseVo:"sys/user/findAllUserInfoBaseVo",
  getRolesByUserIdNotHave:"sys/user/getRolesByUserIdNotHave",
  getSysUserBaseInfoVo:"/sys/user/getSysUserBaseInfoVo",
  getUserRolesByUserId:"sys/user/getUserRolesByUserId",
  modifyUserRolesByUserId:"sys/user/modifyUserRolesByUserId",
  modifyAccountPwdByUserId:"sys/user/modifyAccountPwdByUserId",
  modifySysUserBaseInfoByUserId:"sys/user/modifySysUserBaseInfoByUserId",
  getAllPermsBySysUserId:"sys/user/getAllPermsBySysUserId",
  findAllWithdrawPwdResetApply:"sys/user/findAllWithdrawPwdResetApply",
  findAllPwdResetApply:"sys/user/findAllPwdResetApply",
  confirmPwdResetApply:"sys/user/confirmPwdResetApply",
  confirmWithdrawPwdResetApply:"sys/user/confirmWithdrawPwdResetApply",
  refusedWithdrawPwdResetApply:"sys/user/refusedWithdrawPwdResetApply",
  refusedPwdResetApply:"sys/user/refusedPwdResetApply",
  findAllCheckInReward:"sys/user/findAllCheckInReward",
  reConfirmCheckInReward:"sys/user/reConfirmCheckInReward",
  confirmCheckInReward:"sys/user/confirmCheckInReward",
}

var Options={
}


var permUrl={
  addSysPerm:"sys/perm/addSysPerm",
  findAllSysPerms:"sys/perm/findAllSysPerms",
  getAllRolePermsByRoleId:"sys/perm/getAllRolePermsByRoleId",
  getAllSysPermTreeMarkByRoleId:"/sys/perm/getAllSysPermTreeMarkByRoleId",
  modifySysPerm:"sys/perm/modifySysPerm",
  modifyRolePerms:"sys/perm/modifyRolePerms",
}

var roleUrl={
  findAllRoles:"sys/role/findAllRoles",
  addSysRole:"sys/role/addRole",
  modifyRole:"sys/role/modifyRole",
  delRole:"sys/role/delRole",
}

var menuUrl={
  addSysMenu:"sys/menu/addSysMenu",
  findAllSysMenus:"sys/menu/findAllSysMenus",
  getAllMenusTreeByAccount:"/sys/menu/getAllMenusTreeByAccount",
  getAllSysMenuTreeMarkByRoleId:"sys/menu/getAllSysMenuTreeMarkByRoleId",
  getAllSysMenus:"sys/menu/getAllSysMenus",
  modifyRoleMenus:"sys/menu/modifyRoleMenus",
  modifySysMenu:"sys/menu/modifySysMenu",
  delSysMenu:"sys/menu/delSysMenu",
}

var sysAgentUrl={
}

var fileUrl = {
  uploadSingleHeadImage:"sys/file/uploadSingleHeadImage",
  uploadSysBankImage:"sys/file/uploadSysBankImage",
  uploadGameImage:"sys/file/uploadGameImage",
  uploadClientBgImage:"sys/file/uploadClientBgImage",
  uploadWebLogoImage:"sys/file/uploadWebLogoImage",
  getAllSysLogFile:"sys/file/getAllSysLogFile",
  delAllSysLogFile:"sys/file/delAllSysLogFile"
}
var taskUrl = {
  findAllTask:"sys/task/findAllTask",
  addSysTask:"sys/task/addTask",
  pauseSysTask:"sys/task/pauseTask", //未使用
  resumeSysTask:"sys/task/resumeTask",//未使用
  lockSysTask:"sys/task/lockTask",
  modifyTask:"sys/task/modifyTask",
}
var systemConfigUrl = {
  findAllSysConfig:"sys/config/findAllSysConfig",
  addConfig:"sys/config/addConfig",
  modifyConfigById:"sys/config/modifyConfigById",
  saveClientBgImageInfo:"sys/config/saveClientBgImageInfo",
  saveClientBgImageInfoPC:"sys/config/saveClientBgImageInfoPC",
  saveWebLogo:"sys/config/saveWebLogo",
  findAllAccessLog:"sys/log/findAllAccessLog",
  delAccessLogBeforeDay:"sys/log/delAccessLogBeforeDay"
}
var sysbankUrl = {
  addSysBank:"sys/bank/addBank",
  findAllSysBank:"sys/bank/findAllSysBank",
  modifySysBank:"sys/bank/modifySysBank",
  delSysBank:"sys/bank/delSysBank",
  findAllMemberBanks:"sys/bank/findAllMemberBanks"
}
var sysBannerUrl = {
    addSysBanner:"sys/banner/addSysBanner",
    modifySysBanner:"sys/banner/modifySysBanner",
    findAllSysBanner:"sys/banner/findAllSysBanner"
}
var sysInformationUrl = {
  addSysInformation:"sys/information/addSysInformation",
  modifySysInformation:"sys/information/modifySysInformation",
  findAllSysInformation:"sys/information/findAllSysInformation"
}
var sysAssetUrl = {
  findAllMemberAssetChangeLog:"sys/asset/findAllMemberAssetChangeLog",
  findAllMemberAssetDetail:"sys/asset/findAllMemberAssetDetail",
  adjustMemberAsset:"sys/asset/adjustMemberAsset",
}
var URLS={};
var interfaces={
  openUrl,
  userUrl,
  permUrl,
  roleUrl,
  menuUrl,
  fileUrl,
  taskUrl,
  sysAssetUrl,
  sysbankUrl,
  systemConfigUrl,
  sysBannerUrl,
  sysInformationUrl,
  imUrl,
  Options,
  };

for(var k in interfaces){
  for(var j in interfaces[''+k+'']){
    URLS[''+j+'']=interfaces[''+k+''][''+j+''];
  }
}
// console.log(URLS);

export default URLS;
