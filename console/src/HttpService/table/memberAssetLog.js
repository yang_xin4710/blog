export default {
  userName: {
    params:{
      value:"",
      column:"userName",
      condition:"LIKE",
      prefix:"`t_sys_user`."
    },
    orderBy:{
      state:0,
      index:0,
      value:'',
      options:[]
    },
    alias:'包含',
    state:0,
    label:"用户名",
    type:1,
    conditions:{
      options:[
        {opt:"=",alias:"等于"},
        {opt:"LIKE",alias:"包含"}
      ]
    }
  },
  serialNo: {
    params:{
      value:"",
      column:"serialNo",
      condition:"LIKE",
      prefix:""
    },
    orderBy:{
      state:0,
      index:0,
      value:'',
      options:[]
    },
    alias:'包含',
    state:0,
    label:"流水号",
    type:1,
    conditions:{
      options:[
        {opt:"=",alias:"等于"},
        {opt:"LIKE",alias:"包含"}
      ]
    }
  },
  type: {
    params:{
      value:"",
      column:"type",
      condition:"=",
      prefix:""
    },
    orderBy:{
      state:0,
      index:0,
      value:'',
      options:[]
    },
    alias:'',
    state:0,
    label:"记录类型",
    type:3,
    conditions:{
      options:[
        {opt:"0",alias:"新建记录"},
        {opt:"1",alias:"签到奖励"},
        {opt:"7",alias:"公众号分享奖励"},
        {opt:"999",alias:"积分调整"},
      ]
    }
  },
  createTime: {
    params: {
      column: "createTime"
    },
    orderBy: {
      state: 1,
      value: 'desc',
      options: [
        {opt: 'desc', alias: '降序'},
        {opt: 'asc', alias: '升序'}
      ]
    },
    label: "账变时间",
    type: 2,//时间筛选类型
    filter: [
      {
        params: {
          value: "",
          column: "createTime",
          condition: ">=",
          prefix: "`t_sys_member_asset_log`.",
        },
        alias: '大于等于',
        state: 0,
        value: "00:00:00",
        label: "开始时间",
        conditions: {
          options: [
            {opt: ">=", alias: "大于等于"},
            {opt: "=", alias: "等于"},
            {opt: ">", alias: "大于"}
          ]
        }
      },
      {
        params: {
          value: "",
          column: "createTime",
          condition: "<=",
          prefix: "`t_sys_member_asset_log`.",
        },
        alias: '小于等于',
        state: 0,
        label: "结束时间",
        value: "23:59:59",
        conditions: {
          options: [
            {opt: "<=", alias: "小于等于"},
            {opt: "=", alias: "等于"},
            {opt: "<", alias: "小于"}
          ]
        }
      }
    ]
  }
};
