package com.jtw.conf.shiro.enums;


/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2018/9/3 14:44
 */
public enum  UserState {

    Normal(0, "正常"),
    Lock(1, "锁定");

    private int state;
    private String desc;

    UserState(int state, String desc) {
        this.state = state;
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }
}
