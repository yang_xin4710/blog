package com.jtw.common.baseService;

import com.github.pagehelper.Page;
import com.jtw.common.bean.vo.FindPagingVo;
import com.jtw.common.bean.vo.PagingVo;
import org.quartz.SchedulerException;

import java.util.List;

/**
 * DESCRIPT:公共service
 *
 * @author cjsky666
 * @date 2018-8-29 14:24
 */
public interface BaseService<T> {
    public void save(T t);
    public void saveAll(List<T> list);
    public void update(T t);
    public void delete(Object id);
    public T getById (Object id);
    public Page<T> findAll (PagingVo pagingVo);
    public Page<T> findAll (FindPagingVo findPagingVo);
    public List<T> getAll ();
}
