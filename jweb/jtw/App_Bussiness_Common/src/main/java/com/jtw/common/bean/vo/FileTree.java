package com.jtw.common.bean.vo;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * DESCRIPT: 文件夹目录树模型
 *
 * @author cjsky666
 * @date 2019/5/15 10:12
 */
@Data
public class FileTree {
    private String name;
    private String path;
    private String size;
    private List<FileTree> subTree =new ArrayList<FileTree>();
    private Integer isDirectory;
}
