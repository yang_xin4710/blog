package com.jtw.common.anno.findPagingAnno;

import java.lang.annotation.ElementType;

/**
 * DESCRIPT:筛选条件的过滤注解。
 *   conditions 允许的比对条件
 *   columns 允许的实体属性字段
 *   prefix 查询字段的表前缀
 *   fixedValue 指定允许传输的参数值
 *   isString 标识参数是否为字符串。判断sql拼接是否需要接单引号
 *   notNull 是否必传，不可为空。
 * @author cjsky666
 * @date 2018/11/5 17:53
 */
public @interface FindPagingItemAnno {
    String column();
    String [] conditions() default {"="};
    String prefix() default "";
    String [] fixedValue() default {};
    boolean isString() default true;
    boolean notNull() default false;
}
