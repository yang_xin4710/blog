package com.jtw.common.util;

import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.OperatingSystem;
import eu.bitwalker.useragentutils.UserAgent;

import javax.servlet.http.HttpServletRequest;

/**
 * DESCRIPT:设备类型解析工具类
 *
 * @author cjsky666
 * @date 2018/10/9 17:14
 */
public class DeviceUtil {
    /**
     * 获取请求头中的user-Agent
     * @param request
     * @return
     */
    public static UserAgent getByRequest(HttpServletRequest request){
        return UserAgent.parseUserAgentString(request.getHeader("User-Agent"));
    }
    /**
     * 根据user-Agent 获取浏览器
     * @param request
     * @return
     */
    public static Browser getBrowserByUserAgent(UserAgent userAgent){
        return userAgent.getBrowser();
    }
    /**
     * 根据user-Agent 获取操作设备的操作系统
     * @param request
     * @return
     */
    public static OperatingSystem getOperatingSystemByUserAgent(UserAgent userAgent){
        return userAgent.getOperatingSystem();
    }
}
