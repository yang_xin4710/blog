package com.jtw.common.bean.vo;

import com.google.common.base.CaseFormat;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * DESCRIPT: 通用筛选条件类,记录筛选的条件
 *
 * @author cjsky666
 * @date 2018/10/31 16:40
 */
@Data
public class FindParamVo {
    public static final String [] CONDITIONS = {"=","<",">","<=",">=","<>","LIKE"};
    private String column;//字段
    private String condition;//筛选条件
    private String value;//值
    private Boolean isString = true;//是否为参数类型是否为字符串，默认为true
    private String prefix = "";//table的别名。默认为""
    private Boolean notNull = false;//table的别名。默认为""
    public static final boolean isContains(String condition) {
        for (String s : CONDITIONS) {
            if (s.equals(condition)) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println(CaseFormat.UPPER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, "userName")); // returns "constantName"
    }
}
