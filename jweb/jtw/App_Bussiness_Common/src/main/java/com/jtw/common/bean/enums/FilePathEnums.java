package com.jtw.common.bean.enums;

import com.google.common.base.Enums;
import com.jtw.common.constants.CommonSysConstant;

/**
 * DESCRIPT: 上传文件目录设定类
 *
 * @author cjsky666
 * @date 2018/11/19 9:50
 */
public enum FilePathEnums {
    webSiteLogoImageUploadPath("/image/webSiteLogoImage"),
    headImageUploadPath("/image/headImage"),
    PayImageUploadPath("/image/payImage"),
    OtherImageUploadPath("/image/otherImage"),
    FileExcelUploadPath("/file/excel"),
    FileVideoUploadPath("/file/video"),
    ;
    private String path;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    FilePathEnums(String path) {

        this.path = path;
    }
}
