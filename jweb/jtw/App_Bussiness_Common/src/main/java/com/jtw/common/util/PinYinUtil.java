package com.jtw.common.util;

import com.github.stuxuhai.jpinyin.PinyinException;
import com.github.stuxuhai.jpinyin.PinyinFormat;
import com.github.stuxuhai.jpinyin.PinyinHelper;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * DESCRIPT: 汉字=》拼音转换工具，
 * 符号和非汉字不进行转换，直接拼接
 * @author cjsky666
 * @date 2019/5/28 15:48
 */
public class PinYinUtil {
    /**
     * 过滤错误的地区名字，并替换
     */
    public static Map<String,String> chineseFilter = new HashMap<>();
    static{
        chineseFilter.put("朩","木");
    }
    /**
     * 汉字拼音首字母--大写
     * @param chinese
     * @return
     */
    public static String ToFirstCharUpperCase(String chinese){
        return ToFirstCharLowerCase(chinese).toUpperCase();
    }

    /**
     * 汉字拼音首字母--小写
     * @param chinese
     * @return
     */
    public static String ToFirstCharLowerCase(String chinese){
        String pinyin ="";
        char [] chrs =transformCharArray(chinese).toCharArray();
        for(char s:chrs){
            if(Validator.isChineseInString(String.valueOf(s))){
                pinyin+=(PinyinHelper.convertToPinyinArray(s,PinyinFormat.WITHOUT_TONE)[0].substring(0,1));
            }else{
                pinyin+=(String.valueOf(s));
            }
        }
        return pinyin;
    }

    /**
     * 汉字转为拼音--小写
     * @param chinese
     * @return
     */
    public static String ToPinyinLowerCase(String chinese){
        String pinyin ="";
        char [] chrs = transformCharArray(chinese).toCharArray();
        for(char s:chrs){
            if(Validator.isChineseInString(String.valueOf(s))){
                pinyin+=(PinyinHelper.convertToPinyinArray(s,PinyinFormat.WITHOUT_TONE)[0]);
            }else{
                pinyin+=(String.valueOf(s));
            }
        }
        return pinyin;
    }
    /**
     * 汉字转为拼音----大写
     * @param chinese
     * @return
     */
    public static String ToPinyinUpperCase(String chinese){
        return ToPinyinLowerCase(chinese).toUpperCase();
    }

    /**
     * 汉字转为全拼音----首字母大写
     * @param chinese
     * @return
     */
    public static String ToPinyinFirstCharUpperCase(String chinese){
        String pinyin ="";
        try {
            String [] temp   = PinyinHelper.convertToPinyinString(transformCharArray(chinese)," ",PinyinFormat.WITHOUT_TONE).split(" ");
            for(String s:temp){
                if(s.length()>0){
                    pinyin+=(toUpperCase(s.substring(0,1))+ s.substring(1,s.length())+" ");
                }else{
                    pinyin+=(toUpperCase(s)+" ");
                }
            }
        } catch (PinyinException e) {
            e.printStackTrace();
        }
        return pinyin.substring(0,pinyin.length()-1);
    }



    /**
     * 汉字转为全拼音----首字母小写
     * @param chinese
     * @return
     */
    public static String ToPinyinFirstCharLowwerCase(String chinese){
        String pinyin ="";
        try {
            pinyin = PinyinHelper.convertToPinyinString(transformCharArray(chinese)," ",PinyinFormat.WITHOUT_TONE);
        } catch (PinyinException e) {
            e.printStackTrace();
        }
        return pinyin;
    }

    /**
     * 过滤特殊字符 分解中文汉字
     * @return
     */
    public static String transformCharArray(String chinese){
//        chinese = Validator.filterStringMark(chinese);
        Set<String> strings = chineseFilter.keySet();
        for (String s:strings){
            if(chinese.indexOf(s)>-1){
                chinese = chinese.replaceAll(s,chineseFilter.get(s));
            }
        }
        return  chinese;
    };

    /**
     * 将字母转换成大写
     * @param pinyin
     * @return
     */
    public static String toUpperCase(String pinyin){
        return pinyin.toUpperCase();
    };

    public static void main(String[] args) {
        System.out.println(ToFirstCharUpperCase("水朩清华社区"));
//        System.out.println(ToFirstCharLowerCase("柳港园A社区居委会"));
//        System.out.println(ToPinyinFirstCharUpperCase("幸福ｅ家社区居委会"));
//        System.out.println(ToPinyinFirstCharUpperCase("幸福家、社区居委会"));
//        System.out.println(ToPinyinFirstCharLowwerCase("柳港园A社区居委会"));
//        System.out.println(ToPinyinUpperCase("柳港园、社区居委会"));
//        System.out.println(ToPinyinLowerCase("柳港园A社区居委会"));

    }
}
