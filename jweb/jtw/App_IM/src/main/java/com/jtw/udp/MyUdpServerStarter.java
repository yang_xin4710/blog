package com.jtw.udp;

import org.tio.core.udp.UdpServer;
import org.tio.core.udp.UdpServerConf;

import java.net.SocketException;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/3/22 15:29
 */
public class MyUdpServerStarter {

    public static void main(String[] args) throws SocketException {
        MyUdpHandler udpHandler = new MyUdpHandler();
        UdpServerConf udpServerConf = new UdpServerConf(3000,udpHandler,5000);
        UdpServer udpServer = new UdpServer(udpServerConf);
        udpServer.start();
    }
}
