package com.jtw.constant;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/4/6 0:01
 */
@Data
public class IM_Message implements Serializable {
    private static final long serialVersionUID = 302730681824876158L;
    /**
     * 接收人（字符串用户id，或者）
     * 如果是系统广播的话，为空
     * 如果是群聊 则是groupid
     */
    private String rid;
    private String rname;//接收者昵称
    /**
     * 发送人sid,
     * 如果是广播的话。这个也可以是空
     */
    private String sid;
    private String sname;//发送者昵称
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date sendTime = new Date();
    private Object ctx;
    /**
     * 默认未读
     */
    private Integer readStatus = 0;
}
