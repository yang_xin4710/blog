package com.jtw.udp;

import lombok.extern.slf4j.Slf4j;
import org.tio.core.Node;
import org.tio.core.udp.UdpPacket;
import org.tio.core.udp.intf.UdpHandler;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/3/22 11:35
 */
@Slf4j
public class MyUdpHandler implements UdpHandler {
    @Override
    public void handler(UdpPacket udpPacket, DatagramSocket datagramSocket) {
        byte[] data = udpPacket.getData();
        String msg = new String(data);
        Node remote = udpPacket.getRemote();
        log.info("收到来自{}的消息:【{}】", remote, msg);
        DatagramPacket datagramPacket = new DatagramPacket(data, data.length, new InetSocketAddress(remote.getIp(), remote.getPort()));
        try {
            datagramSocket.send(datagramPacket);
        } catch (Throwable e) {
            log.error(e.toString(), e);
        }
    }
}
