package com.jtw.sys.service.banner;

import com.github.pagehelper.Page;
import com.jtw.common.bean.enums.ReqCode;
import com.jtw.common.bean.vo.FindPagingVo;
import com.jtw.common.bean.vo.PagingVo;
import com.jtw.common.util.PagingUtil;
import com.jtw.sys.mapper.banner.SysBannerMapper;
import com.jtw.sys.model.banner.TSysBanner;
import com.jtw.sys.vo.banner.SysBannerVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/2/21 13:57
 */
@Service
public class SysBannerServiceImpl implements SysBannerService {
    @Autowired
    private SysBannerMapper sysBannerMapper;
    @Override
    public void save(TSysBanner tSysBanner) {
        sysBannerMapper.insertSelective(tSysBanner);
    }

    @Override
    public void saveAll(List<TSysBanner> list) {

    }

    @Override
    public void update(TSysBanner tSysBanner) {
        if(!sysBannerMapper.existsWithPrimaryKey(tSysBanner.getId())){
            throw ReqCode.UnExistingDataException.getException();
        }
        if(tSysBanner.getSkipWay()==0){
            tSysBanner.setUrl(" ");
        }
        sysBannerMapper.updateByPrimaryKeySelective(tSysBanner);
    }

    @Override
    public void delete(Object id) {

    }

    @Override
    public TSysBanner getById(Object id) {
        return null;
    }

    @Override
    public Page<TSysBanner> findAll(PagingVo pagingVo) {
        return null;
    }

    @Override
    public Page<TSysBanner> findAll(FindPagingVo findPagingVo) {
        PagingUtil.start(findPagingVo);
        return sysBannerMapper.findAll(PagingUtil.buildWhereSQLToString(findPagingVo.getFindParams()));
    }

    @Override
    public List<TSysBanner> getAll() {
        return null;
    }

    @Override
    public List<SysBannerVo> getAllMobileClientBanner() {
        return sysBannerMapper.getAllMobileClientBanner();
    }
}
