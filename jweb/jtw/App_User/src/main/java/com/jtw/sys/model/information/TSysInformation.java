package com.jtw.sys.model.information;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/2/21 11:31
 */
@Data
@Table(name = "`t_sys_information`")
public class TSysInformation implements Serializable {
    private static final long serialVersionUID = 4297200396935161097L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID() from dual")
    private Integer id;
    private String author;
    private String title;
    private Integer state;
    private String content;
    private String descript;
    private String operator;
    private String operatorIp;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date updateTime;
}
