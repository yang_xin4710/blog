package com.jtw.sys.service.log;

import com.github.pagehelper.Page;
import com.jtw.common.baseService.BaseService;
import com.jtw.common.bean.vo.FindPagingVo;
import com.jtw.common.bean.vo.PagingVo;
import com.jtw.common.util.DeviceUtil;
import com.jtw.common.util.IpUtil;
import com.jtw.sys.mapper.log.SysLoginMapper;
import com.jtw.sys.model.log.TSysUserLoginLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * DESCRIPT: 系统用户登录记录表
 *
 * @author cjsky666
 * @date 2018/9/21 13:08
 */
@Service
public class SysLoginServiceImpl implements BaseService<TSysUserLoginLog> {
    @Autowired
    private SysLoginMapper sysLoginMapper;
    @Override
    public void save(TSysUserLoginLog tSysUserLoginLog) {
        sysLoginMapper.insertSelective(tSysUserLoginLog);
    }

    @Override
    public void saveAll(List<TSysUserLoginLog> list) {

    }

    @Override
    public void update(TSysUserLoginLog tSysUserLoginLog) {

    }

    @Override
    public void delete(Object id) {

    }

    @Override
    public TSysUserLoginLog getById(Object id) {
        return null;
    }

    @Override
    public Page<TSysUserLoginLog> findAll(PagingVo pagingVo) {
        return null;
    }

    @Override
    public Page<TSysUserLoginLog> findAll(FindPagingVo findPagingVo) {
        return null;
    }

    @Override
    public List<TSysUserLoginLog> getAll() {
        return null;
    }

    public void saveLoinLog(HttpServletRequest request,Long userId,Integer mode){
        TSysUserLoginLog sysUserLoginLog = new TSysUserLoginLog();
        sysUserLoginLog.setIp(request.getRemoteAddr());
        sysUserLoginLog.setUserId(userId);
        sysUserLoginLog.setLoginMode(mode);
        sysUserLoginLog.setDevice(DeviceUtil.getOperatingSystemByUserAgent(DeviceUtil.getByRequest(request)).getName());
        sysUserLoginLog.setAddress(IpUtil.getIpFromAttribution(request.getRemoteAddr()));
        sysLoginMapper.insertSelective(sysUserLoginLog);
    }
}
