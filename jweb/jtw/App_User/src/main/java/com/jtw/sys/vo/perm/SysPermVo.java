package com.jtw.sys.vo.perm;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * DESCRIPT: 映射列表系统权限业务模型
 *
 * @author cjsky666
 * @date 2018/9/13 10:29
 */
@Data
public class SysPermVo implements Serializable {
    private static final long serialVersionUID = -8419332924983576795L;
    @NotBlank(message = "权限名称不能为空")
    private String name;

    @NotNull(message = "权限类型")
    private Integer type;

    @NotBlank(message = "权限映射路径")
    private String url;

    @NotBlank(message = "权限备注不能为空")
    private String desc;

    @NotBlank(message = "请求类型不能为空")
    private String httpMethod;

    @NotBlank(message = "类方法名不能为空")
    private String methodName;

    @NotBlank(message = "类包名不能为空")
    private String className;

    private Integer group;

    private Integer sort;

    @NotNull(message = "权限状态必须设置")
    private Integer state;

    @NotNull(message = "是否公共接口必须设置")
    private Boolean open;
}
