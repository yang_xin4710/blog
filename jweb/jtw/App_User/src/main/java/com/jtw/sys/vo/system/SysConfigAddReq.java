package com.jtw.sys.vo.system;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/1/18 16:21
 */
@Data
public class SysConfigAddReq implements Serializable {
    private static final long serialVersionUID = -7895503945567875186L;
    @NotBlank(message = "键不能为空")
    private String keyName;
    @NotBlank(message = "值不能为空")
    private String keyValue;
    @NotBlank(message = "描述不能为空")
    private String descript;
}
