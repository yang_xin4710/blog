package com.jtw.sys.vo.information;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * DESCRIPT: 资讯业务类
 *
 * @author cjsky666
 * @date 2019/2/21 11:58
 */
@Data
public class SysInformationVo implements Serializable {
    private static final long serialVersionUID = 443242671215243494L;
    private Integer id;
    private String author;
    private String title;
    private Integer state;
    private String content;
    private String descript;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date updateTime;
}
