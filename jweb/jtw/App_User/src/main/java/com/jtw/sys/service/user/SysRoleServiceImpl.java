package com.jtw.sys.service.user;

import com.github.pagehelper.Page;
import com.jtw.common.bean.enums.ReqCode;
import com.jtw.common.bean.vo.FindPagingVo;
import com.jtw.common.bean.vo.PagingVo;
import com.jtw.common.util.PagingUtil;
import com.jtw.conf.shiro.util.ShiroUtil;
import com.jtw.sys.constants.Constant;
import com.jtw.sys.mapper.user.SysRoleMapper;
import com.jtw.sys.model.user.TSysRole;
import com.jtw.sys.service.perm.AuthService;
import com.jtw.sys.vo.role.SysRoleUpdateVo;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * DESCRIPT:系统角色Service
 *
 * @author cjsky666
 * @date 2018/9/17 16:34
 */
@Service
public class SysRoleServiceImpl implements SysRoleService {
    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Autowired
    private ShiroFilterFactoryBean shiroFilterFactoryBean;

    @Autowired
    private AuthService authService;

    @Override
    public void save(TSysRole tSysRole) {
        sysRoleMapper.insertSelective(tSysRole);
    }

    @Override
    public void saveAll(List<TSysRole> list) {

    }

    @Override
    public void update(TSysRole tSysRole) {
        sysRoleMapper.updateByPrimaryKeySelective(tSysRole);
    }

    @Override
    public void delete(Object id) {
        sysRoleMapper.deleteByPrimaryKey(id);
    }


    @Override
    public TSysRole getById(Object id) {
        return null;
    }



    @Override
    public Page<TSysRole> findAll(PagingVo pagingVo) {
        return null;
    }

    @Override
    public Page<TSysRole> findAll(FindPagingVo findPagingVo) {
        return null;
    }

    @Override
    public List<TSysRole> getAll() {
        return null;
    }

    public Page<SysRoleUpdateVo> findAllRoles(PagingVo pagingVo) {
        PagingUtil.start(pagingVo);
        return sysRoleMapper.findAllRoles();
    }

    @Override
    @Transactional
    public void deleteAllOfThisRolePerms(Integer roleId) {
        if(Constant.ROLE_ID_SUPER_ADMIN==roleId){
            throw ReqCode.ForbidDeleteAdminRole.getException();
        }else if(Constant.ROLE_ID_MEMBER==roleId){
            throw ReqCode.ForbidDeleteAdminRole.getException();
        }
        sysRoleMapper.deleteRoleWithUser(roleId);
        sysRoleMapper.deleteRoleWithPerm(roleId);
        sysRoleMapper.deleteByPrimaryKey(roleId);
        try {
            ShiroUtil.updateFilterChain(shiroFilterFactoryBean, authService);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
