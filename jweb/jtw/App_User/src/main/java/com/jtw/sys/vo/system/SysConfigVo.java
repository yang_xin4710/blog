package com.jtw.sys.vo.system;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/1/18 16:21
 */
@Data
public class SysConfigVo implements Serializable {
    private static final long serialVersionUID = -5860597631846203512L;
    private Integer id;
    private String keyName;
    private String keyValue;
    private String descript;
    private String operator;
    private String operatorIp;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date updateTime;
}
