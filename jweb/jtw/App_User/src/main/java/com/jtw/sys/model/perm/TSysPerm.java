package com.jtw.sys.model.perm;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * DESCRIPT:shiro用户权限
 * @author cjsky666
 * @date 2018/9/14 14:48
 */
@Data
@Table(name = "`t_sys_perm`")
public class TSysPerm  implements Serializable {
    private static final long serialVersionUID = 1053811365668150158L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID() from dual")
    private Integer id;

    private String name;

    private Integer type;

    private String url;

    private String desc;

    private String httpMethod;

    private String methodName;

    private String className;

    private Boolean open;

    private Integer group;

    private Integer sort;

    private Integer state;

    private String operator;

    private String operatorIp;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date updateTime;
}