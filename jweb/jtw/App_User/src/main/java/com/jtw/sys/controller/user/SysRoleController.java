package com.jtw.sys.controller.user;

import com.jtw.common.anno.RequestAnno.PermAnno;
import com.jtw.common.bean.Result.Message;
import com.jtw.common.bean.enums.ReqCode;
import com.jtw.common.bean.vo.PagingVo;
import com.jtw.sys.model.user.TSysRole;
import com.jtw.sys.service.user.SysRoleServiceImpl;
import com.jtw.sys.vo.role.SysRoleAddReq;
import com.jtw.sys.vo.role.SysRoleUpdateReq;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * DESCRIPT:系统角色管理
 *
 * @author cjsky666
 * @date 2018/9/17 16:20
 */
@Api(tags = "系统角色管理模块")
@RestController
@Slf4j
@Validated
@RequestMapping(value = "/sys/role")
public class SysRoleController {
    @Autowired
    private SysRoleServiceImpl sysRoleServiceImpl;

    @ApiOperation(notes = "系统角色管理",value = "查询所有系统角色")
    @GetMapping(value="/findAllRoles")
    @PermAnno(sort = 1,group=4)
    public Message findAllSysRoles(PagingVo pagingVo){
        return ReqCode.Success.getMessage(sysRoleServiceImpl.findAllRoles(pagingVo));
    }

    @ApiOperation(notes = "系统角色管理",value = "修改单个系统角色")
    @PostMapping(value="/modifyRole")
    @PermAnno(sort = 2,group=4)
    public Message modifyRole(@Valid SysRoleUpdateReq sysRoleUpdateReq){
        TSysRole sysRole = new TSysRole();
        BeanUtils.copyProperties(sysRoleUpdateReq,sysRole);
        sysRoleServiceImpl.update(sysRole);
        return ReqCode.UpdateSuccess.getMessage();
    }

    @ApiOperation(notes = "系统角色管理",value = "添加单个系统角色")
    @PostMapping(value="/addRole")
    @PermAnno(sort = 3,group=4)
    public Message addRole(@Valid SysRoleAddReq sysRoleAddReq){
        TSysRole sysRole = new TSysRole();
        BeanUtils.copyProperties(sysRoleAddReq,sysRole);
        sysRoleServiceImpl.save(sysRole);
        return ReqCode.AddSuccess.getMessage();
    }
    @ApiOperation(notes = "系统角色管理",value = "删除单个系统角色")
    @PostMapping(value="/delRole")
    @PermAnno(sort = 4,group=4)
    public Message delRole(@NotNull(message = "roleId不能为空") Integer roleId){
        sysRoleServiceImpl.deleteAllOfThisRolePerms(roleId);
        return ReqCode.DelSuccess.getMessage();
    }
}
