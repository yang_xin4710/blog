package com.jtw.sys.vo.asset;

import lombok.Data;

import java.io.Serializable;

/**
 * DESCRIPT: 系统会员资产信息业务类
 *
 * @author cjsky666
 * @date 2019/2/4 0:13
 */
@Data
public class SysMemberAssetUpdateReq  implements Serializable {
    private static final long serialVersionUID = 3394783920042699582L;
    private Integer id;
    private Integer balance;
    private Integer freezeBalance;
}
