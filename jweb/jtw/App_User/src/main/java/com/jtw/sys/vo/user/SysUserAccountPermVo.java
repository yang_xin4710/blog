package com.jtw.sys.vo.user;

import com.jtw.sys.vo.perm.SysRolePermAddVo;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * DESCRIPT: 系统账号所拥有的角色及其权限列表
 *
 * @author cjsky666
 * @date 2018/10/23 23:09
 */
@Data
public class SysUserAccountPermVo implements Serializable {
    private static final long serialVersionUID = -8618091421156183932L;
    private Integer id;
    private String name;
    private String desc;
    private List<SysRolePermAddVo> perms;


}


