package com.jtw.sys.service.perm;

import com.jtw.common.baseService.BaseService;
import com.jtw.sys.model.perm.TSysPermGroup;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2018/9/12 14:02
 */
public interface SysPermGroupService extends BaseService<TSysPermGroup> {
}
