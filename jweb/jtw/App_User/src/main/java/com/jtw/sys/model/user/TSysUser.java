package com.jtw.sys.model.user;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.File;
import java.io.Serializable;
import java.util.Date;

/**
 * DESCRIPT:shiro用户表
 *
 * @author cjsky666
 * @date 2018/9/14 14:48
 */

@Data
@Table(name = "`t_sys_user`")
public class TSysUser implements Serializable {

    private static final long serialVersionUID = -5401037056794869623L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID() from dual")
    private Long id;
    private String userName;
    private String password;
    private String salt;
    private Integer state;
    private String operator;

    private String operatorIp;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date updateTime;
}