//package com.jtw.sys.task.timeTask;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.scheduling.annotation.EnableScheduling;
//import org.springframework.scheduling.annotation.Scheduled;
//
//import java.util.Calendar;
//
///**
// * DESCRIPT:这是一个定时任务
// *
// * @author cjsky666
// * @date 2018-8-22 14:36
// */
//@Configuration
//@EnableScheduling
//@Slf4j
//public class taskLogPrint {
//
//    @Scheduled(cron = "0/120 * * * * ?")
//    void loginfo(){
//       log.information("currentTime "+Calendar.getInstance().getTimeInMillis());
//    }
//}
