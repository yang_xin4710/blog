package com.jtw.sys.mapper.task;

import com.MyMapper;
import com.github.pagehelper.Page;
import com.jtw.sys.model.task.TSysTask;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2018/11/23 10:09
 */
public interface SysTaskMapper extends MyMapper<TSysTask> {
    @Select("SELECT * FROM `t_sys_task` ${whereSQL}")
    Page<TSysTask> findAll(@Param("whereSQL") String whereSQL);
    @Select("SELECT * FROM `t_sys_task` WHERE `GAME_ID` = #{gameId}")
    TSysTask getOneByGameId(@Param("gameId") Integer gameId);
}
