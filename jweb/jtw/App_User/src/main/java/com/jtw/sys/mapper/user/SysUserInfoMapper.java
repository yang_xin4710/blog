package com.jtw.sys.mapper.user;

import com.MyMapper;
import com.jtw.sys.model.user.TSysUserInfo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * DESCRIPT:用户基本信息DAO
 *
 * @author cjsky666
 * @date 2018/9/21 17:05
 */
public interface SysUserInfoMapper extends MyMapper<TSysUserInfo> {
}
