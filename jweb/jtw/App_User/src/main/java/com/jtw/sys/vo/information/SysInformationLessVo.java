package com.jtw.sys.vo.information;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/3/24 1:52
 */
@Data
public class SysInformationLessVo implements Serializable {
    private static final long serialVersionUID = -1868677951456934794L;
    private Integer id;
    private String author;
    private String title;
    private String descript;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date updateTime;
}
