package com.jtw.sys.service.member;

import com.github.pagehelper.Page;
import com.jtw.common.bean.enums.ReqCode;
import com.jtw.common.bean.vo.FindPagingVo;
import com.jtw.common.bean.vo.PagingVo;
import com.jtw.common.org.Sid;
import com.jtw.common.util.PagingUtil;
import com.jtw.conf.shiro.util.PasswordUtil;
import com.jtw.sys.constants.Constant;
import com.jtw.sys.emuns.AssetChangeCode;
import com.jtw.sys.mapper.member.MemberAssetLogMapper;
import com.jtw.sys.mapper.member.MemberAssetMapper;
import com.jtw.sys.model.member.TSysMemberAsset;
import com.jtw.sys.model.member.TSysMemberAssetLog;
import com.jtw.sys.vo.asset.SysMemberAssetUpdateReq;
import com.jtw.sys.vo.asset.SysMemberAssetVo;
import com.jtw.sys.vo.member.MemberAssetLogVo;
import com.jtw.sys.vo.member.MemberAssetRankVo;
import com.jtw.sys.vo.member.MemberAssetVo;
import com.jtw.sys.vo.member.SysMemberAssetLogVo;
import org.apache.shiro.SecurityUtils;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/1/19 20:45
 */
@Service
public class MemberAssetServiceImpl implements MemberAssetService {
    @Autowired
    private MemberAssetMapper memberAssetMapper;
    @Autowired
    private Sid sid;
    @Autowired
    private MemberAssetLogMapper memberAssetLogMapper;
    @Override
    public void save(TSysMemberAsset tSysMemberAsset) {

    }

    @Override
    public void saveAll(List<TSysMemberAsset> list) {

    }

    @Override
    public void update(TSysMemberAsset tSysMemberAsset) {

    }

    @Override
    public void delete(Object id) {

    }

    @Override
    public MemberAssetVo getOneByUserId(Long userId) {
        return memberAssetMapper.getOneByUserId(userId);
    }

    @Override
    public TSysMemberAsset getById(Object id) {
        return memberAssetMapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<TSysMemberAsset> findAll(PagingVo pagingVo) {
        return null;
    }

    @Override
    public Page<TSysMemberAsset> findAll(FindPagingVo findPagingVo) {
        return null;
    }

    @Override
    public List<TSysMemberAsset> getAll() {
        return null;
    }

    @Override
    public Page<MemberAssetLogVo> findAllMyAssetLog(Long userId, FindPagingVo findPagingVo) {
        PagingUtil.start(findPagingVo);
        return memberAssetMapper.findAllMyAssetLog(PagingUtil.buildCustomeWhereSQLToString("AND `USER_ID` = " + userId,findPagingVo.getFindParams()));
    }

    @Override
    public Page<SysMemberAssetLogVo> findAllMemberAssetChangeLog(FindPagingVo findPagingVo) {
        PagingUtil.start(findPagingVo);
        return memberAssetMapper.findAllMemberAssetChangeLog(PagingUtil.buildWhereSQLToString(findPagingVo.getFindParams()));
    }
    @Override
    public Page<SysMemberAssetVo> findAllMemberAssetDetail(FindPagingVo findPagingVo) {
        PagingUtil.start(findPagingVo);
        return memberAssetMapper.findAllMemberAssetDetail(PagingUtil.buildWhereSQLToString(findPagingVo.getFindParams()));
    }

    @Override
    @Transactional
    public void adjustMemberAsset(HttpServletRequest request, SysMemberAssetUpdateReq sysMemberAssetUpdateReq) {
        TSysMemberAsset memberAssetVo = memberAssetMapper.selectByPrimaryKey(sysMemberAssetUpdateReq.getId());
        if(memberAssetVo==null){
            throw ReqCode.UnExistingDataException.getException();
        }
        //修改资金信息
        memberAssetVo.setBalance(sysMemberAssetUpdateReq.getBalance());
        memberAssetVo.setFreezeBalance(sysMemberAssetUpdateReq.getFreezeBalance());
        if(memberAssetMapper.updateByVersion(memberAssetVo)!=1){
            throw ReqCode.DataLockedUpdateError.getException();
        }

        //生成账变日志
        TSysMemberAssetLog tSysMemberAssetLog = new TSysMemberAssetLog();
        tSysMemberAssetLog.setBalance(0);
        tSysMemberAssetLog.setAssetFreezeBalance(memberAssetVo.getFreezeBalance());
        tSysMemberAssetLog.setAssetBalance(memberAssetVo.getBalance());
        tSysMemberAssetLog.setOperatorIp(request.getRemoteAddr());
        tSysMemberAssetLog.setOperator((String) request.getSession().getAttribute(Constant.KEY_USER_USER_NAME));
        tSysMemberAssetLog.setUserId(memberAssetVo.getId().longValue());
        tSysMemberAssetLog.setSerialNo(sid.nextShort());
        tSysMemberAssetLog.setType(AssetChangeCode.ADJUST_BALANCE.getType());
        tSysMemberAssetLog.setDescript(AssetChangeCode.ADJUST_BALANCE.getDescript());
        memberAssetLogMapper.insertSelective(tSysMemberAssetLog);
    }

    @Override
    public Page<MemberAssetRankVo> findAllRank(PagingVo pagingVo) {
        PagingUtil.start(pagingVo);
        Page<MemberAssetRankVo> allRank = memberAssetMapper.findAllRank();
        return allRank;
    }

    @Override
    public MemberAssetRankVo getMyAssetRank(Integer userId) {
        return memberAssetMapper.getMyAssetRank(userId);
    }

    @Override
    @Transactional
    public void wxgzhShareCallBack(Long userId) {
        if(memberAssetLogMapper.checkTodayWxgzhShare(userId,LocalDate.now().toDate())==0){
            TSysMemberAsset asset = memberAssetMapper.selectByPrimaryKey(userId);
            asset.setBalance(asset.getBalance()+Integer.valueOf(Constant.sysConfMap.get(Constant.KEY_WXGZH_SHARE_REWARD).toString()));
            memberAssetMapper.updateByVersion(asset);
            TSysMemberAssetLog tSysMemberAssetLog = new TSysMemberAssetLog();
            tSysMemberAssetLog.setBalance(Integer.valueOf(Constant.sysConfMap.get(Constant.KEY_WXGZH_SHARE_REWARD)));
            tSysMemberAssetLog.setAssetFreezeBalance(asset.getFreezeBalance());
            tSysMemberAssetLog.setAssetBalance(asset.getBalance());
            tSysMemberAssetLog.setOperatorIp("0.0.0.0");
            tSysMemberAssetLog.setOperator((String) SecurityUtils.getSubject().getSession().getAttribute(Constant.KEY_USER_USER_NAME));
            tSysMemberAssetLog.setUserId(userId);
            tSysMemberAssetLog.setSerialNo(sid.nextShort());
            tSysMemberAssetLog.setType(AssetChangeCode.ADD_SHARE.getType());
            tSysMemberAssetLog.setDescript(AssetChangeCode.ADD_SHARE.getDescript());
            memberAssetLogMapper.insertSelective(tSysMemberAssetLog);
        }
    }
}
