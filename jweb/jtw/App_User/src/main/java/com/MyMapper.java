package com;
import com.github.pagehelper.Page;
import com.jtw.sys.model.system.TSysConfig;
import com.jtw.sys.model.task.TSysTask;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * DESCRIPT:继承自己的MyMapper,实现通用是CURD功能
 *
 * @author cjsky666
 * @date 2018-8-23 18:38
 */
public interface MyMapper<T> extends Mapper<T>, MySqlMapper<T> {
    //TODO
    //FIXME 特别注意，该接口不能被扫描到，否则会出错
}
