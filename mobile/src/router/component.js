const Container = () => import ('@/components/Container');
const hello00 = () => import ('@/components/HelloWorld00')
const v_login = () => import ('@/components/login/Login')
const v_sign = () => import ('@/components/login/Sign')
const v_information = () => import('@/components/information/information')
const v_userInfo = () => import ('@/components/account/info/userInfo')
const v_MemberPwd = () => import ('@/components/account/pwd/pwd')
const v_cacheView = () => import('@/views/cache/view_Cache')
/**
 * URI资源对应的路由组件,
 * @type {{path: string, name: string, component: (function(): (Promise<*>|*)), children: *[]}}
 */
const URI = {
  path: '/home',
  name: '中心',
  component: Container,
  children: []
}

const MemberPwd = {
  name: "密码管理",
  path: "/home/account/pwd",
  component: v_MemberPwd,
  children: []
}
const UserInfo = {
  name: "个人资料",
  path: "/home/info/userInfo",
  component: v_userInfo,
  children: []
}

const CacheView = {
  name: "缓存案例",
  path: "/cache",
  component: v_cacheView,
  children: [
    {
      name:"页签懒加载主页",
      path:"/cache/tabs",
      component: () => import("@/views/cache/view_Cache_Tabs"),
      children: []
    }
  ]
}


export default [
  {
    path: '/',
    redirect: '/home'
  },
  URI,
  UserInfo,
  MemberPwd,
  {
    path: '/login',
    component: v_login,
    name: "登录"
  },
  {
    path: '/info/information',
    component: v_information,
    name: "资讯详情"
  },
  CacheView,
  {
    path: '/sign',
    component: v_sign,
    name: "会员注册"
  },
  {
    path: '/404',
    component: hello00,
    name: "互联网的荒原"
  },
  {
    path: '*',
    redirect: "/404"
  }
]
