import 'lib-flexible/flexible'
if (Number.parseInt === undefined) Number.parseInt = window.parseInt;
if (Number.parseFloat === undefined) Number.parseFloat = window.parseFloat;
import App from './App';
import router from './router/routerscfg';
import store from '@/vuex/store';
import api from '@/HttpService/api';
import util from '@/utils/pro_util';
import PRO_CONSTANT from '@/utils/PRO_CONSTANT';

Vue.config.productionTip = false;
Vue.prototype.proAPI = api;
Vue.prototype.proUtil = util;

Vue.prototype.PRO_CONSTANT = PRO_CONSTANT;
//引入了之后需要先全局 手动注册vant 的懒加载模块
// Vue.use(vant.Lazyload);

new Vue({
  el: '#app',
  router,
  store,
  components: {
    App
  },
  render: h => h(App)
})
