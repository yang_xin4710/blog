/**
 * 微信分享工具脚本
 * 处理微信二次分享 朋友圈，聊天框，qq，微博，QQ空间
 */
var localShareTypes = ['onMenuShareTimeline', 'onMenuShareAppMessage', 'onMenuShareQQ', 'onMenuShareWeibo','onMenuShareQZone'];

var clearWxshareUrl= function(){
  var shareurl='';
  var params_str = window.location.search;
  var t_str = '';
  var flag=false;//标志当前页面是否需要跳转一次
  if(params_str!=''&&params_str!=null){
    params_str = params_str.substring(1,params_str.length).split("&");//先截取？号 再切割为数组.区分朋友圈携带的参数。处理二次带参数转发导致的转发失效问题
    for(var i = 0;i<params_str.length;i++){
      var t_arr = params_str[i].split('=');
      if(t_arr[0]=='from'&&(t_arr[1]=='singlemessage'||t_arr[1]=='singlemessage'||t_arr[1]=='groupmessage')){
        flag=true;
      }else{
        t_str+=(params_str[i]+"&");
      }
    }
    t_str="?"+t_str;//补全url的问号
    t_str = t_str.substring(0,t_str.length-1);//接去掉最后一个&号
  }
  shareurl = location.protocol+"//"+location.hostname+window.location.pathname+t_str;
  if(flag){
    window.location.replace(shareurl);
  }
}
/**
 * 调用微信二次分享方法清除分享标识
 */
clearWxshareUrl();//处理微信二次分享的问题
/**
 * 微信分享初始化
 * @constructor
 */
var WxshareInit= function(path){
  var shareurl='';
  var params_str = window.location.search;
  var t_str = '';
  var flag=false;//标志当前页面是否需要跳转一次
  if(params_str!=''&&params_str!=null){
    params_str = params_str.substring(1,params_str.length).split("&");//先截取？号 再切割为数组.区分朋友圈携带的参数。处理二次带参数转发导致的转发失效问题
    for(var i = 0;i<params_str.length;i++){
      var t_arr = params_str[i].split('=');
      if(t_arr[0]=='from'&&(t_arr[1]=='singlemessage'||t_arr[1]=='singlemessage'||t_arr[1]=='groupmessage')){
        flag=true;
      }else{
        t_str+=(params_str[i]+"&");
      }
    }
    t_str="?"+t_str;//补全url的问号
    t_str = t_str.substring(0,t_str.length-1);//接去掉最后一个&号
  }
  shareurl = encodeURIComponent(location.protocol+"//"+location.hostname+window.location.pathname+t_str);
  if(!flag){
    return axios.get(path+"?url="+shareurl,{withCredentials:true});//处理自定义分享路径。因为get提交会自动&的转化amp;需要转换
  }
}
/**
 *
 * @param sharecfg 分享签名数据包 用于注册微信分享功能
 * @param shareurl 分享的链接地址
 * @param dev_flag 是否开发模式
 */
var setWxshareInfo=function(sharecfg,dev_flag,shareshowinfo){
  var cfg = {
    debug : dev_flag,
    appId : sharecfg.data.appId,
    timestamp : sharecfg.data.timestamp,
    nonceStr : sharecfg.data.nonceStr,
    signature : sharecfg.data.signature,
    jsApiList : localShareTypes
  }
  wx.config(cfg);
  wx.ready(function () {
    wx.onMenuShareAppMessage(shareconfig.shareShareAppMessage);
    wx.onMenuShareTimeline(shareconfig.ShareTimeline);
    wx.onMenuShareQQ(shareconfig.ShareQQ);
    wx.onMenuShareWeibo(shareconfig.ShareWeibo);
    wx.onMenuShareQZone(shareconfig.ShareQZone);
  })
}

/**
 *  分享的回调接口实体
 * @type {{url: string, bean: {dxfid: string, dxtype: string, dxurl: string, dxfid: string, zftj: string, zfid: number}, cb: webshareBean.cb}}
 */
var webshareBean = {
  url:'',
  bean:{
    shareToken:'',
    shareType:'',
    shareLink:''
  },
  cb:function(obj){
    axios.post(webshareBean.url,filterParamsURLSearchParams(obj),{withCredentials:true}).then(function(res){
    }).catch(function(err){
    })
  }
}

//微信分享的配置项
var shareconfig={
  ShareTimeline: {
    title: '',    // 分享标题
    desc: '',      // 分享描述
    link: '',             // 分享链接
    imgUrl: '',  // 分享图标
    shareToken: '',  // 分享回调操作的令牌
    complete: function (res) {
    },
    success: function (res) {
      shareCallBackFunc.timeLine();
    },
    cancel: function (res) {
    },
    fail: function (res) {
    }
  },
  shareShareAppMessage: {
    title: '',    // 分享标题
    desc: '',      // 分享描述
    link: '',             // 分享链接
    imgUrl: '',  // 分享图标
    shareToken: '',  // 分享回调操作的令牌
    complete: function (res) {
    },
    success: function (res) {
      shareCallBackFunc.appMessage();
    },
    cancel: function (res) {
    },
    fail: function (res) {
    }
  },
  ShareQQ: {
    title: '',    // 分享标题
    desc: '',      // 分享描述
    link: '',             // 分享链接
    imgUrl: '',  // 分享图标
    shareToken: '',  // 分享回调操作的令牌
    complete: function (res) {
    },
    success: function (res) {
      shareCallBackFunc.QQ();
    },
    cancel: function (res) {
    },
    fail: function (res) {
    }
  },
  ShareWeibo: {
    title: '',    // 分享标题
    desc: '',      // 分享描述
    link: '',             // 分享链接
    imgUrl: '',  // 分享图标
    shareToken: '',  // 分享回调操作的令牌
    complete: function (res) {
    },
    success: function (res) {
      shareCallBackFunc.weibo();
    },
    cancel: function (res) {
    },
    fail: function (res) {
    }
  },
  ShareQZone: {
    title: '',    // 分享标题
    desc: '',      // 分享描述
    link: '',             // 分享链接
    imgUrl: '',  // 分享图标
    shareToken: '',  // 分享回调操作的令牌
    complete: function (res) {
    },
    success: function (res) {
      shareCallBackFunc.QZone();
    },
    cancel: function (res) {
    },
    fail: function (res) {
    }
  }
}

/**
 * obj 为页面自定义对象
 * 自动生成微信分享的config
 */
var CreateshareConfig= function(obj){
  for(var key in shareconfig){
    shareconfig[''+key+''].title=obj.title;
    shareconfig[''+key+''].desc=obj.desc;
    shareconfig[''+key+''].link=obj.link;
    shareconfig[''+key+''].imgUrl=obj.imgUrl;
    shareconfig[''+key+''].shareToken=obj.shareToken;
    shareconfig[''+key+''].cbUrl=obj.cbUrl;
  }
}
//分享成功之后的回调函数
var shareCallBackFunc = {
  timeLine:function(){
    sharesuccesscallbackfunc("ShareTimeline");
  },
  appMessage:function(){
    sharesuccesscallbackfunc("shareShareAppMessage");
  },
  QQ:function(){
    sharesuccesscallbackfunc("ShareQQ");
  },
  weibo:function(){
    sharesuccesscallbackfunc("ShareWeibo");
  },
  QZone:function(){
    sharesuccesscallbackfunc("ShareQZone");
  }
}

var sharesuccesscallbackfunc = function (type) {
  webshareBean.bean.shareToken = shareconfig[""+type+""].shareToken;
  webshareBean.bean.shareLink = shareconfig[""+type+""].link;
  webshareBean.bean.shareType = type;
  webshareBean.url = shareconfig[""+type+""].cbUrl;
  webshareBean.cb(webshareBean.bean);
}
/**
 * POST过滤当前需要传递的请求JSON参数URLSearchParams
 *并且设定了初始值不能传递过去的规则
 */
var filterParamsURLSearchParams=function(params){
  var p = new URLSearchParams();
  for(var key in params){
    if(params[''+key+'']!=null&&params[''+key+'']!=''&&params[''+key+'']!='originalvalue'){
      p.append(key, params[''+key+'']);
    }
  }
  return p;
}
